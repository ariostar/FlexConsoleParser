#FlexConsoleParser

这是一个Eclipse插件。功能为检测Console输出，将Flex错误标识为文件链接。
主要用于配合Ant方式编译Flex。

项目创建于Eclipse3.2，其它版本Eclipse能否使用未知。
