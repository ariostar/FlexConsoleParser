/**
 * 
 */
package flexconsoleparser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author lizhiyu
 *
 */
public class FlexErrorLineParser {
	
	private String fMessage;
	private String fFile_Name;
	private int fLine_Number;
	private int fColumn_Number;
	private boolean fIsFound = false;
	private Matcher fMatcher;
	
	protected Pattern fPattern;
	
	private static final String PATTERN_FILE = 
		"(([a-z]:|\\\\\\\\[a-z0-9]+)\\\\([^/:*?\"<>|\\r\\n]*\\\\)?[^\\\\/:*?\"<>|\\r\\n]*)";
	private static final String PATTERN_MESSAGE = "\\((\\d+)\\):(\\s+col:\\s+(\\d+))?\\s+(.*)";
	private static final String PATTERN_LINE = '^' + PATTERN_FILE + PATTERN_MESSAGE + '$';

	
	public FlexErrorLineParser() {
		clear();
		fPattern = Pattern.compile(PATTERN_LINE, Pattern.CASE_INSENSITIVE);
	}
	
	public void clear() {
		fMessage = "";
		fFile_Name = "";
		fLine_Number = -1;
		fColumn_Number = 0;
		fIsFound = false;
	}
	
	public void parse(String line) {
		clear();
		
		fMatcher = fPattern.matcher(line);
	}
	
	public boolean find() {
		fIsFound = fMatcher.find();
		
		if (fIsFound) {
			fFile_Name = fMatcher.group(1);
			fLine_Number = Integer.parseInt(fMatcher.group(4));
			String sVal = fMatcher.group(6);
			if (sVal != null) {
				fColumn_Number = Integer.parseInt(sVal);
			}
			fMessage = fMatcher.group(7);
		}
		
		return fIsFound;
	}
	
	public String getFileName() {
		return fFile_Name;
	}
	
	public String getMessage() {
		return fMessage;
	}
	
	public int getLineNumber() {
		return fLine_Number;
	}
	
	public int getColumnNumber() {
		return fColumn_Number;
	}
}

