package flexconsoleparser;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class ParserPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "FlexConsoleParser";

	// The shared instance
	private static ParserPlugin plugin;
	
	/**
	 * The constructor
	 */
	public ParserPlugin() {
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ParserPlugin getDefault() {
		return plugin;
	}
	
	/**
	 * make a IStatus from throwable
	 * @param message
	 * @param exception
	 * @return
	 */
	public static IStatus newErrorStatus(String message, Throwable exception) {
	  return new Status(IStatus.ERROR, PLUGIN_ID, 120, message, exception);
	}
	
	
	/**
	 * log throwable for this plugin
	 * @param t
	 */
	protected static void log(Throwable t) {
		getDefault().getLog().log(newErrorStatus("Error logged from " + PLUGIN_ID + ": ", t));
	}

}
